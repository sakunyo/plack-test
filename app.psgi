#!/usr/bin/env perl

use strict;
use warnings;
use Plack::Builder;

my $app = sub {
  my $env = shift;

  return [
    200,
    ['Content-Type'=>'text/plain'],
    ["\n"]
  ];
};

$app = builder {
	enable 'Static',
		path => qr!^/!,
		root => 'static/';
	$app;
};
