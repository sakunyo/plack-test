(function () {
  "use strict";

  var View = (function () {

    var View = function () {
      this.element = document.querySelector(".p");
    };

    View.prototype = {
      _hide: function () {
        var self = this;
        Velocity(this.element,
          { opacity: 0 },
          { complete: function () { self._destroy(); } });
      },

      _destroy: function () {
        this.element.style.display = "none";
        console.log("done");
      },

      move: function () {
        var self = this;
        Velocity(this.element,
          { top: 50, left: "50%" },
          { complete: function () { self._hide(); } });
      }
    };

    return View;
  })();

  var view = new View();

  setTimeout(function () {
    view.move();
  }, 2000);

})();